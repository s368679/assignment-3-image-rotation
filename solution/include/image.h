#pragma once

#include <stdint.h>
#include <stdlib.h>

struct pixel { uint8_t b, g, r; };
struct image {
  uint64_t width, height;
  struct pixel* data;
};

#define IMAGE_ERROR (struct image) {0}


struct image create_image(uint64_t width, uint64_t height);
void destroy_image(struct image* res);

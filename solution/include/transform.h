#pragma once

#include "image.h"

struct image rotate90( struct image *source );
struct image rotate( struct image source, int angle );

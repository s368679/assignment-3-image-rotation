#pragma once

#include "image.h"
#include "stdio.h"
#include  <stdint.h>

#pragma pack(push, 1)
struct bmp_header 
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};
#pragma pack(pop)


enum error_status  {
  ERROR_OK = 0,
  ERROR_INVALID_SIGNATURE,
  ERROR_INVALID_BITS,
  ERROR_INVALID_HEADER
};

enum error_status from_bmp( FILE* in, struct image* img );

enum error_status to_bmp( FILE* out, struct image const* img );

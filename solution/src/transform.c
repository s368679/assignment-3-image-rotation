#include "transform.h"

struct image rotate90( struct image *source ) {
	struct image rotated = create_image(source->height, source->width);
	for (uint64_t i = 0; i < source->width; i++){
		for (uint64_t k = 0; k < source->height; k++){
			rotated.data[source->height * i + k] = source->data[source->width * (source->height - k - 1) + i];
        }
    }
	return rotated;
}

struct image rotate( struct image source, int angle ) {
    if (angle == 0)
        return source;
    struct image result = source;
    for (int i = 0; i < 4 - angle/90; i++) {
        struct image prev = result;
        result = rotate90(&result);
        destroy_image(&prev);
    }
    return result;
}

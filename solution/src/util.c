#include "util.h"

void print_error(enum error_status error, const char *f) {
    fprintf(stderr, "Ошибка файла: %s - ", f);
    switch (error) {
        case ERROR_INVALID_SIGNATURE:
           fprintf(stderr, "Неверная сигнатура"); break;
        case ERROR_INVALID_BITS:
           fprintf(stderr, "Неверные биты"); break;
        case ERROR_INVALID_HEADER:
           fprintf(stderr, "Неверный заголовок"); break;
        default: break;
    }
    fprintf(stderr, "\n");
}
void error_exit(const char *mess) {
  perror(mess);
  exit(EXIT_FAILURE);
}

#include "image.h"

struct image create_image(uint64_t width, uint64_t height) {
    struct image result;
    result.data = malloc(width * height * sizeof(struct pixel));
    if (result.data == NULL)
        return IMAGE_ERROR;
    result.width = width;
    result.height = height;
    return result;
}


void destroy_image(struct image* res) {
    if (res->data != NULL) {
        free(res->data);
        res->data = NULL;
        res->width = res->height = 0;
    }
}

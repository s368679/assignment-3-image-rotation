#include "bmp.h"


#define BMP_TYPE 0x4D42
#define BF_RESERVED 0
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_COMPRESSION 0
#define BI_CLR_USED 0
#define BI_CLR_IMPORTANT 0

#define BYTE_TO_BITS 8

uint8_t get_padd(uint64_t width) {
    uint8_t padding = 4 - width * sizeof(struct pixel) % 4;
    return (padding % 4 != 0 ? padding : 0);
}

enum error_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header;
    unsigned long read = fread(&header, sizeof(struct bmp_header), 1, in);
    if (read != 1)
        return ERROR_INVALID_HEADER;
    else if (header.bfType != BMP_TYPE)
        return ERROR_INVALID_SIGNATURE;

    uint8_t padding = get_padd(header.biWidth);
    *img = create_image(header.biWidth, header.biHeight);
    if (img->width == 0)
        return ERROR_INVALID_BITS;
    
    for (uint64_t i = 0; i < img->height; i++) {
        if (fread(img->data + i * img->width, sizeof(struct pixel), img->width, in) != img->width)
            return ERROR_INVALID_BITS;
        else if (padding > 0){
            unsigned long result = fseek(in, padding , SEEK_CUR);
            if (result != 0) {
                return ERROR_INVALID_BITS;
            }
        }
    }

    return ERROR_OK;
}

enum error_status to_bmp( FILE* out, struct image const* img ) { 
    uint8_t padding = get_padd(img->width);
    
    struct bmp_header header = {0};
    header.bfType = BMP_TYPE;
    header.bfileSize = sizeof(struct bmp_header) + img->height * (img->width * sizeof(struct pixel) + padding);
    header.bfReserved = BF_RESERVED;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = BI_SIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = BI_PLANES;
    header.biBitCount = sizeof(struct pixel) * BYTE_TO_BITS;
    header.biCompression = BI_COMPRESSION;
    header.biSizeImage = img->height * img->width * sizeof(struct pixel);
    header.biClrUsed = BI_CLR_USED;
    header.biClrImportant = BI_CLR_IMPORTANT;
    
    unsigned long write = fwrite(&header, sizeof(struct bmp_header), 1, out);
    if (write == 0)
        return ERROR_INVALID_HEADER;
    
    uint8_t values[3] = {0};
    for (uint64_t i = 0; i < img->height; i++) {
        if (fwrite(img->data + i * img->width, sizeof(struct pixel) , img->width, out) != img->width)
            return ERROR_INVALID_BITS;
        else if (padding > 0){
            unsigned long result = fwrite(&values, sizeof(padding), padding, out);
            if (result != padding) {
                return ERROR_INVALID_BITS;
            }
        } 
    }
    return ERROR_OK;
}


#include "bmp.h"
#include "ctype.h"
#include "stdbool.h"
#include "transform.h"
#include "util.h"

#define INPUT_ARG 1
#define OUTPUT_ARG 2
#define ANGLE_ARG 3
#define NUMBER_ARGUMENTS 4

bool is_numeric(char *str) {
    if (*str == '-' || *str == '+') {
        str++;
    }
    while (*str) {
        if (!isdigit(*str)) {
            return false;
        }
        str++;
    }
    return true;
}

int main( int argc, char** argv ) {
    if (argc != NUMBER_ARGUMENTS) {
        error_exit( "Неверное количество аргументов!");
    }
    if (!is_numeric(argv[ANGLE_ARG])) {
        error_exit("Неверный угол поворота! Разрешены только целочисленные углы кратные 90!\n");
    }
    int angle = (atoi(argv[ANGLE_ARG]) % 360 + 360) % 360;
    if (angle % 90 != 0) {
        error_exit("Неверный угол поворота! Разрешены только углы кратные 90!\n");
    }
    
    const char *inputted_str = argv[INPUT_ARG];
    const char *outputted_str = argv[OUTPUT_ARG];
    FILE *inputted = fopen(inputted_str, "rb");
    FILE *outputted = fopen(outputted_str, "wb");
    
    if (inputted == NULL || outputted == NULL) {
        error_exit("Файл неверен!\n");
    }
    
    struct image inputted_image;
    enum error_status read_status = from_bmp(inputted, &inputted_image);
    fclose(inputted);
    if (read_status != ERROR_OK) {
        destroy_image(&inputted_image);
        fclose(outputted);
        print_error(read_status, inputted_str);
        return 1;
    }
    
    struct image outputted_image = rotate( inputted_image , angle );

    enum error_status write_status = to_bmp(outputted, &outputted_image);
    fclose(outputted);

    //destroy_image(&inputted_image);
    destroy_image(&outputted_image);
    
    if (write_status != ERROR_OK) {
        print_error(write_status, outputted_str);
        return 1;
    }
    
    return 0;
}
